Definir Funcao VerificarFornecedorOC(Numero CodigoFornecedor);
Definir Funcao ExibirMensagemFornecedorErroOC(Numero CodigoFornecedor);
Definir Alfa ComandoAuxiliar;
Definir Alfa CursorAuxiliar;
Definir Alfa SituacaoFornecedor;
Definir Alfa aCodigoFornecedor;

Se (CodEmp = 630)
Inicio
    VerificarFornecedorOC(VsCodFor);
Fim;

Funcao VerificarFornecedorOC(Numero CodigoFornecedor);
Inicio
    ComandoAuxiliar = " \
            SELECT USU_SitFor FROM E095HFO \
            WHERE CodFor = :nCodFor \
            AND CodEmp = 630 \
        ";
    Sql_Criar(CursorAuxiliar);
    Sql_DefinirComando(CursorAuxiliar,ComandoAuxiliar);
    SQL_DefinirInteiro(CursorAuxiliar, "nCodFor", CodigoFornecedor);
    SQL_AbrirCursor(CursorAuxiliar);
    Se (SQL_EOF(CursorAuxiliar) = 0)
    Inicio
        SQL_RetornarAlfa(CursorAuxiliar, "USU_SitFor", SituacaoFornecedor);
        Se (SituacaoFornecedor <> "A")
        Inicio
            ExibirMensagemFornecedorErroOC(CodigoFornecedor);
        Fim;
    Fim;
    SQL_FecharCursor(CursorAuxiliar);
    SQL_Destruir(CursorAuxiliar);
Fim;

Funcao ExibirMensagemFornecedorErroOC(Numero CodigoFornecedor);
Inicio
    IntParaAlfa(CodigoFornecedor, aCodigoFornecedor);
    aCodigoFornecedor = "Fornecedor " + aCodigoFornecedor + " precisa ser requalificado";
    Mensagem(Erro, aCodigoFornecedor);
Fim;