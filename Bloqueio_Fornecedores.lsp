/* 
 * Desenvolvimento do processo de inativação de fornecedores por ordem de compra.
*/
Definir Funcao Contrutor();
Definir Funcao EncontrarFornecedores();
Definir Funcao EncontrarUltimaOrdemCompra();
Definir Funcao VerificarData(Numero DataEmissao, Numero End AcaoRealizada);
Definir Funcao InativarFornecedor();
Definir Funcao ConverterVariaveisParaTexto();
Definir Funcao GravarLogObservacao(Numero Operacao);
Definir Alfa ComandoAuxiliar;
Definir Alfa CursorAuxiliar;
Definir Alfa aTextoObservacao;
Definir Alfa aTextoObservacaoFornecedor;
Definir Alfa aMensagemErro;
Definir Alfa aNumOcp;
Definir Alfa aCodFor;
Definir Alfa aDatEmi;
Definir Data dDatEmi;
Definir Numero nCodFor;
Definir Numero nNumOcp;
Definir Numero nPosicaoAtual;
Definir Data DataAtual;
Definir Numero HoraAtual;
Definir Data Data24Meses;
Definir Numero nFilial;
Definir Numero nCodUsu;
Definir Lista ListaFornecedores;

Contrutor();
EncontrarFornecedores();
EncontrarUltimaOrdemCompra();

Funcao Contrutor();
Inicio
    ListaFornecedores.DefinirCampos();
    ListaFornecedores.AdicionarCampo("CodFor", Numero);
    ListaFornecedores.AdicionarCampo("CodAct", Numero); @ Ação realizada: 0 - Não realizou nenhum procedimento, 1 - Inativou @ 
    ListaFornecedores.EfetivarCampos();
    
    nCodUsu = 1139;
    HoraAtual = HorSis;
    Data24Meses = DatSis - 730; @ 730 dias corresponde a 24 meses ou 2 anos @
    DataAtual = DatSis;
    AlfaParaInt(Filial, nFilial);
    Se(nFilial = 0)
        nFilial = 1;
Fim;

Funcao EncontrarFornecedores();
Inicio   
    ComandoAuxiliar = " \
        SELECT DISTINCT(E095FOR.CodFor) CodFor, E095HFO.USU_MotSit ObsMot FROM E095FOR, E095HFO \
        WHERE E095FOR.CodFor = E095HFO.CodFor \
        AND E095HFO.USU_SitFor = 'A' \
        AND E095HFO.CodEmp = 630 \
        AND (E095FOR.CodFor IN( \
            SELECT DISTINCT(E095FOR.CodFor) FROM E095FOR, E403FPR, E095HFO, E075PRO \
            WHERE E095FOR.CodFor = E095HFO.CodFor \
            AND E095FOR.CodFor = E403FPR.CodFor \
            AND E075PRO.CodPro = E403FPR.CodPro \
            AND E095HFO.CodEmp = 630 \
            AND (E075PRO.CodFam IN('1201', '1204', '1301', '1399', '1401', '1407', '2901', '2908') \
            OR (E075PRO.CodFam >= '3001' AND E075PRO.CodFam <= '3099')) \
            OR E095FOR.CodFor IN( \
            SELECT DISTINCT(E095FOR.CodFor) FROM E095FOR, E403FSE, E095HFO, E080SER \
            WHERE E095FOR.CodFor = E095HFO.CodFor \ 
            AND E095FOR.CodFor = E403FSE.CodFor \
            AND E080SER.CodSer = E403FSE.CodSer \
            AND E095HFO.CodEmp = 630 \
            AND E080SER.Usu_ReqReq = 'S')) \
        ) "; 
    Sql_Criar(CursorAuxiliar);
    Sql_DefinirComando(CursorAuxiliar,ComandoAuxiliar);
    SQL_AbrirCursor(CursorAuxiliar);
    Enquanto(SQL_EOF(CursorAuxiliar) = 0)
    Inicio
        SQL_RetornarInteiro(CursorAuxiliar, "CodFor", nCodFor);
        SQL_RetornarAlfa(CursorAuxiliar, "ObsMot", aTextoObservacaoFornecedor);
        ListaFornecedores.Adicionar();
        ListaFornecedores.CodFor = nCodFor;
        ListaFornecedores.CodAct = 0;
        ListaFornecedores.Gravar();
        SQL_Proximo(CursorAuxiliar);
    Fim;
    SQL_FecharCursor(CursorAuxiliar);
    SQL_Destruir(CursorAuxiliar);
    ComandoAuxiliar = "";
Fim; 

Funcao EncontrarUltimaOrdemCompra();
Inicio
    ListaFornecedores.Primeiro(); 
    Enquanto(ListaFornecedores.FDA = 0)
    Inicio
        nPosicaoAtual = ListaFornecedores.NumReg + 1;
        nCodFor = ListaFornecedores.CodFor;
        ComandoAuxiliar = " \
            SELECT NumOcp, DatEmi FROM E420OCP \
            WHERE CodFor = :nCodFor \
            AND CodEmp = 630 \
            AND DatEmi = \
                (SELECT MAX(DatEmi) DatEmi FROM E420OCP \
                WHERE CodFor = :nCodFor \
                AND CodEmp = 630)  \
        ";
        Sql_Criar(CursorAuxiliar);
        Sql_DefinirComando(CursorAuxiliar,ComandoAuxiliar);
        SQL_DefinirInteiro(CursorAuxiliar, "nCodFor", nCodFor);
        SQL_AbrirCursor(CursorAuxiliar);
        Se(SQL_EOF(CursorAuxiliar) = 0)
        Inicio
            SQL_Retornarinteiro(CursorAuxiliar, "NumOcp", nNumOcp);
            SQL_RetornarData(CursorAuxiliar, "DatEmi", dDatEmi);
            VerificarData(dDatEmi, nRetorno);
            ListaFornecedores.Editar();
            ListaFornecedores.CodAct = nRetorno;
            ListaFornecedores.Gravar();
        Fim;
        SQL_FecharCursor(CursorAuxiliar);
        SQL_Destruir(CursorAuxiliar);
        ListaFornecedores.Proximo();    
    Fim;
    ComandoAuxiliar = "";
Fim;

Funcao VerificarData(Numero DataEmissao, Numero End AcaoRealizada);
Inicio
    Se(DataEmissao < Data24Meses)
    Inicio
        @ Será necessário inativar o fornecedor @
        AcaoRealizada = 1;
        InativarFornecedor();
    Fim;
    Senao
    Inicio
        @ Nada foi necessário realizar nenhum procedimento @
        AcaoRealizada = 0;
    Fim;
Fim;

Funcao InativarFornecedor();
Inicio
    aMensagemErro = "";
    ConverterVariaveisParaTexto();
    aTextoObservacaoFornecedor = aTextoObservacaoFornecedor + "Fornecedor deve ser requalificado.      Ultima O.C nº: " + aNumOcp + ",      OC Data: " + aDatEmi;
    IniciarTransacao();
    ExecSqlEx("UPDATE E095HFO SET USU_SitFor = 'I', USU_MotSit = :aTextoObservacaoFornecedor WHERE CodFor = :nCodFor AND CodEmp = 630", CodErr, aMensagemErro);
    Se(CodErr = 0)
    Inicio
        FinalizarTransacao();
        GravarLogObservacao(1);
    Fim;
    Senao
    Inicio
        DesfazerTransacao();
        aMensagemErro = "Não foi possível alterar o fornecedor " + aCodFor + " - Causa:" + aMensagemErro;
        GravarLogObservacao(2);
    Fim;
Fim;

Funcao ConverterVariaveisParaTexto();
Inicio
    aDatEmi = "";
    aCodFor = "";
    aNumOcp = "";
    IntParaAlfa(nNumOcp, aNumOcp);
    IntParaAlfa(nCodFor, aCodFor);
    ConverteMascara(3, dDatEmi, aDatEmi, "DD/MM/YYYY");
    LimpaEspacos(aNumOcp);
    LimpaEspacos(aCodFor);
    LimpaEspacos(aDatEmi);
Fim;
@ Operação 1 - Conseguiu alterar na tabela de fornecedor, 2 - Não conseguiu alterar na tabela de fornecedor @
Funcao GravarLogObservacao(Numero Operacao);
Inicio
    Se (Operacao = 1)
    Inicio
        aTextoObservacao = "Fornecedor " + aCodFor + " inativado por requalificação na empresa 630";
    Fim;
    Senao Se(Operacao = 2)
    Inicio 
        aTextoObservacao = "Não foi possível inativar o fornecedor nas definições da empresa 630";
    Fim;
    IniciarTransacao();
    ExecSqlEx("INSERT INTO E000OBS (DatGer, SeqObs, EmpTab, FilTab, NomTab, TipObs, TexObs, ProGer, EmpGer, FilGer, UsuGer) \
    VALUES (:DataAtual, :nPosicaoAtual, 630, 1, 'E095FOR', 'A', :aTextoObservacao, 'Requalific', 630, 1, :nCodUsu)", CodErr, aMensagemErro);
    Se(CodErr = 0)
        FinalizarTransacao();
    Senao
    Inicio
        aMensagemErro = "Não foi possível gravar a observação para o fornecedor " + aCodFor + " - Causa:" + aMensagemErro;
        DesfazerTransacao();
        Mensagem(Erro, aMensagemErro);
    Fim;    
Fim;